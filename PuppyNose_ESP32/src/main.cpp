// choose the type of sensor, only one of the lines below should be active:

#define BME680_I2C_enabled
// end sensor choice

#if !defined(BME680_I2C_enabled) && !defined(BNO055_enabled) && !defined(MPU9250_enabled) && !defined(MPU9250_SPI_enabled)
#error No sensor is defined!
#endif

// choose the board type, only one of the lines below should be active:
//#define M5Stick_enabled //enables use of the screen on the M5 stick
//#define Firebeetle_enabled //enables use of the firebeetle board

#define PuppyESP_enabled

// end board choice
#if !defined(M5Stick_enabled) && !defined(Firebeetle_enabled) && !defined(PuppyESP_enabled)
#error No ESP32 board is defined!
#endif
#if defined(M5Stick_enabled) && defined(Firebeetle_enabled)
#error Two conflicting ESP32 boards are defined!
#endif

// optional sensors:
//#define ABP_enabled //enables use of the Honywell ABP Pressure sensor

#include "CRC16.h" //CRC implementation https://github.com/RobTillaart/CRC

#include <Wire.h>                      //used for I2C communication
#include <InstrumentedObjectHelpers.h> //contains debug output tool
using namespace helpers;               // contains the verbose levels from InstrumentedObjectHelpers.h

#include <BufferSupport.h>    //data buffers
#include <BluetoothSupport.h> //Bluetooth SPP object

#include "esp_adc_cal.h" //used to calibrate the output of the build in ADC channels based on factory calibration in the eeprom

const String deviceName = "Puppy_Nose_01";         // Bluetooth device name
const float versionNumber = 1.16;                  // version displayed on boot
const uint16_t btTimeOut = 1000;                   // bluetooth timeout in ms
BluetoothSupport btSupport(btTimeOut, deviceName); // create a Bluetooth communication class

// Deep sleep support
// https://randomnerdtutorials.com/esp32-deep-sleep-arduino-ide-wake-up-sources/
// Author: Pranav Cherukupalli <cherukupallip@gmail.com>
//#define BUTTON_PIN_BITMASK 0x9C00008000 // dec2hex(2^36+2^39+2^34+2^35+2^15)

#ifdef PuppyESP_enabled // using the PuppyNose with ESP32 PICO MINI 02.

// IO Pins LED
const int PIN_LED_R = 2; // Yellow LED on the PCB

const gpio_num_t i2c_sda = GPIO_NUM_21;         // SDA GPIO21 SLC GPIO22  are the default pins
const gpio_num_t i2c_scl = GPIO_NUM_22;         // display is also connected to this I2C
const gpio_num_t button_s1_GPIO = GPIO_NUM_38;  // power button is connected to this pin  (S1 switch) (enable interal pull-up)
const gpio_num_t button_s2_GPIO = GPIO_NUM_39;  // button S2 (enable interal pull-up)
const gpio_num_t batt_stat1_GPIO = GPIO_NUM_37; // status line one of BQ25171 batt monitor (enable interal pull-up)  0 = error, 1 = ok
const gpio_num_t batt_stat2_GPIO = GPIO_NUM_36; // status line two of BQ25171 batt monitor (enable interal pull-up)  0 = charging, 1 = idle
const uint8_t batt_sw_in_GPIO = 35;             // input line from VBAT_SW (contains half of the battery voltage), enumrates to ADC1_CHANNEL_7
const uint8_t batt_sw_out_GPIO = 25;            // output line from VBAT_SW (enable / disable battery monitor)
const float battCalibrationFactor = 1.155;      // ADC readout / battCalibrationFactor = appr real battery voltage
const uint8_t battLowWaringVoltage = 35;        // volt times 10, if (ADC readout / battCalibrationFactor) results in 3.5 Volt or lower, the batt low warning will be triggered.
const uint8_t battLowSleepVoltage = 30;         // volt times 10, if (ADC readout / battCalibrationFactor) results in 3.0 Volt or lower, the device will be put in deep sleep.

TwoWire PUP_I2C = TwoWire(0);

#include <SSD1803a_I2C.h>      //https://github.com/firexx/SSD1803a_I2C
const byte LCD_i2caddr = 0x3C; // https://www.lcd-module.com/fileadmin/eng/pdf/doma/dogs164e.pdf
SSD1803a_I2C lcd(LCD_i2caddr);

struct Button
{
  const uint8_t PIN;
  bool pressed;
  uint32_t lastPressMillis;
};

Button buttonS1 = {button_s1_GPIO, false, 0};

void IRAM_ATTR button_S1_ISR()
{
  buttonS1.pressed = true;
}

#endif //#PuppyESP_enabled

#ifdef BME680_I2C_enabled // BME gass sensor connected to I2C via CN3 header of the Puppy Nose

#include <BsecSupport.h> //Bosch BSEC lib helpers

// Bsec BME680_I2C;                         // an instance of the BME680 class
const uint8_t BME680_I2C_address = 0x76; // SDO to GND gives 0x76, SDO to VDD gives 0x77

// include library to read and write from flash memory
#include <EEPROM.h>
// define the number of bytes you want to access
const uint8_t EEPROM_SIZE = BSEC_MAX_STATE_BLOB_SIZE;

byte sensorBuffer[16];
byte rawSensorBuffer[16];
byte sensorStatusBuffer[8];

/* Configure the BSEC library with information about the sensor
    18v/33v = Voltage at Vdd. 1.8V or 3.3V
    3s/300s = BSEC operating mode, BSEC_SAMPLE_RATE_LP or BSEC_SAMPLE_RATE_ULP
    4d/28d = Operating age of the sensor in days
    generic_18v_3s_4d
    generic_18v_3s_28d
    generic_18v_300s_4d
    generic_18v_300s_28d
    generic_33v_3s_4d
    generic_33v_3s_28d
    generic_33v_300s_4d
    generic_33v_300s_28d
*/
const uint8_t bsec_config_iaq[] = {
#include "config/generic_33v_3s_4d/bsec_iaq.txt"
};
const String bsec_config_string = "generic_33v_3s_4d"; // manually keep the same as bsec_config_iaq[], used for debug output.

BsecSupport bsecSupport(PIN_LED_R); // create a bsec helper class

#endif //# BME680_I2C_enabled

// https://github.com/arkhipenko/TaskScheduler
#define _TASK_TIMECRITICAL
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_THREAD_SAFE
#define _TASK_SCHEDULING_OPTIONS // Support for multiple scheduling options
#include <TaskScheduler.h>

// the task durations define the sample rate of the tasks:
const uint16_t aquisitionTaskDuration = 1000;  // ms per cycle   //for aquisition of sensor data
const uint8_t communicationTaskDuration = 100; // ms per cycle   //of data transmission to client via Blueooth
const uint8_t jitterMax = 10;                  // after this number of missed samples, the device is stopped.

// Callback methods prototypes used by the task scheduler
void aquisitionTask_Callback();
void communicationTask_Callback();
// Tasks
Task aquisitionTask(aquisitionTaskDuration, -1, &aquisitionTask_Callback);
Task communicationTask(communicationTaskDuration, -1, &communicationTask_Callback);

Scheduler runner; // tasks are added to the scheduler in the setup() sequence and the scheduler is executed in main() loop.

const int sleepTimeOut = 300;                      // number of seconds of idle before the system goes into sleep. Idle is defined as no aquisition running.
const int sleepTimeOutWarning = sleepTimeOut - 60; // minute before sleep, a warning can be given.
const uint8_t powerDownButtonPressTime = 3;        // after this seconds button press device should shutdown

esp_adc_cal_characteristics_t adc_chars;

namespace buffers
{
  // Definition of the data format
  const unsigned char startByteA = 0xFF; // two bytes placed in front of every sample set
  const unsigned char startByteB = 0xFA; // two bytes placed in front of every sample set
  const uint8_t startBytesSize = 2;
  const unsigned char startBytes[startBytesSize] = {startByteA, startByteB};

  const int communicationBufferSize = 10240; // size of the two buffers used for communication towards Bluetooth. Due to a bug, all buffers currently have this size, and only the first few positions are used in most cases.

  // initialize buffer names, they will be defined in the setup();
  SmallBufferSupport bufferCounter;      // counter buffer 24bit, 3x2 byte
  SmallBufferSupport bufferMillis;       // millisecond timer counter 24bit, 3x2 byte
  SmallBufferSupport bufferSensor;       // int32_t temp, int32_t hum, int32_t press, int32_t gas,
  SmallBufferSupport bufferSensorRaw;    // uint32_t temp_raw, uint16_t hum_raw, uint32_t press_raw, uint16_t gas_raw,
  SmallBufferSupport bufferSensorStatus; // uint8_t accuracyCO2, uint8_t accuracyVOC, uint8_t accuracyIAQ, uint8_t accuracyPercentGAS, uint8_t statusInitStab, uint8_t statusPowerOnStab, uint8_t batteryVoltage
  SmallBufferSupport bufferCRC;          // CRC-16 data buffer, 2x2 byte
  SmallBufferSupport bufferSampleSet;    // complete buffer for a single sample set
  LargeBufferSupport btBufferA;          // first "large" buffer used to send the data to the Bluetooth receiver
  LargeBufferSupport btBufferB;          // second "large" buffer used to send the data to the Bluetooth receiver
}
using namespace buffers;

// CRC Check
//  https://github.com/RobTillaart/CRC
CRC16 crc;

// TODO: variables that should be constant or be moved
boolean btConnected = false;             // used to track if BT has been connected, and to give the "BT connected" log message.
int jitterCounter = 0;                   // amount of times the sample momement was more than one sample time off.
uint16_t idleCounter = 0;                // increases in the communication loop (which is always active) and resets in the aquisition loop, or by pressing the power button
unsigned long powerButtonMillisLow = 0;  // time of last falling power button press
unsigned long powerButtonMillisHigh = 0; // time of last rising power button press
unsigned long s2ButtonMillisLow = 0;     // time of last falling power button press
unsigned long s2ButtonMillisHigh = 0;    // time of last rising power button press
bool idleWarningGiven = false;           // used to make sure the warning is given once every time the system is about to shutdown.
RTC_DATA_ATTR int bootCount = 0;         // counts the number of times the system woke up from deep sleep (=shutdown).

uint32_t ADC_A0_VALUE = 0;
uint32_t ADC_A1_VALUE = 0;
uint32_t ADC_A2_VALUE = 0;
uint32_t ADC_A3_VALUE = 0;
uint32_t ADC_A4_VALUE = 0;
uint32_t ADC_A5_VALUE = 0;

uint32_t sampleCounter = 0;
uint32_t sampleCounterOld = 0;
uint32_t millisOld = 0;
uint8_t millisWraps = 0; // count the number of times millis() reached 2^24.

// read ADC channels and convert to mV (based upon https://deepbluembedded.com/esp32-adc-tutorial-read-analog-voltage-arduino/)
uint16_t readADC_ChannelCal(int adcChannelPin)
{
  esp_adc_cal_characteristics_t adc_chars;
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars); // full range 0-3.3V
  uint16_t ADC_VALUE = analogRead(adcChannelPin);
  return ((uint16_t)esp_adc_cal_raw_to_voltage(ADC_VALUE, &adc_chars));
}

// toggles the state of the Blue LED on the IMU Board
void toggleLED(int GPIO)
{
  if (digitalRead(GPIO))
  {
    // digitalWrite(PIN_LED, LOW);   //left out the led on the Firebeetle PCB, in the M5 version it is used for error logging
    digitalWrite(GPIO, HIGH);
  }
  else
  {
    // digitalWrite(PIN_LED, HIGH);
    digitalWrite(GPIO, LOW);
  }
}

/*
Method to put the device into sleep
*/
void goIntoSleep()
{
  helpers::printToLog("Going into sleep.", verboseLevels::info);

#ifdef BNO055_enabled
  // set BNO055 IMU in sleep mode:
  bno.enterSuspendMode();
#endif

#ifdef BME680_I2C_enabled // BME gass sensor connected to I2C via CN3 header of the Puppy Nose

  // TODO put BME in sleep
  // BME680_I2C.reset();

#endif //# BME680_I2C_enabled

#ifdef PuppyESP_enabled // using the PuppyNose with ESP32 PICO MINI 02.
  lcd.setCursor(0, 0);
  lcd.print("Sleeping....          ");
  delay(1000);
  lcd.noDisplay();

#endif // PuppyNose

  delay(500);
  // activate sleep mode. After wakeup, practically it is a reboot of the device. Only the RTC memory is kept.
#if defined(Firebeetle_enabled) || defined(PuppyESP_enabled)
  esp_deep_sleep_start(); // this is the generic ESP32 sleep, the M5 below uses a custom implementation
#endif                    // firebeetle or puppy

#ifdef M5Stick_enabled
  M5.Axp.SetLDO2(true); // should disable screen? //TODO check if this does anything
  M5.Axp.SetLDO3(true); // should disable screen? //TODO check if this does anything
  // M5.Lcd.sleep();
  M5.Axp.DeepSleep(); // deep sleep for M5 Stick (generic ESP32 sleep does not put screen in sleep.)
#endif                // M5

  // this part should not be reached
  helpers::printToLog("Sleep failed!", verboseLevels::error, PIN_LED_R);
}

/*
this taks reads the sensor data at a fixed interval. Based upon the realtime example of library TaskScheduler (https://github.com/arkhipenko/TaskScheduler)
this task will be started and stopped based upon the <start> / <stop> commands received via Bluetooth.
 */
void aquisitionTask_Callback()
{

#if !defined(UART_Force) && !defined(MPU9250_SPI_enabled) // cadance is controlled by the Force ADC providing ~500 Hz Data stream or by IMU chip, jitter detection for the interal scheduler is disabled
  // if (aquisitionTask.getOverrun() != aquisitionTaskDuration)
  // {
  //   if (abs(aquisitionTask.getStartDelay()) > aquisitionTaskDuration) // more than one sample jitter
  //   {
  //     helpers::printToLog("too much overrun = " + String(aquisitionTask.getOverrun()) + ", start delayed by " + String(aquisitionTask.getStartDelay()), verboseLevels::extraTiming, PIN_LED_R);
  //     jitterCounter = jitterCounter + 1; // count the number of times a full sample is missed.
  //   }
  //   else
  //   {
  //     jitterCounter = 0; // reset the counter if a sample is made within the normal sample time.
  //   }
  // }

  // if (jitterCounter > jitterMax)
  // {
  //   helpers::printToLog("too much overrun = " + String(jitterMax) + " times a sample is missed sequentially.", verboseLevels::error, PIN_LED_R);
  //   aquisitionTask.disable();
  //   helpers::printToLog("Stopped aquisition.", verboseLevels::error, PIN_LED_R);
  //   btSupport.bluetoothFlush();

  // }

#endif // !UART Force

  bool succesRead = false; // set to true if Sensor readout was succesfull
  idleCounter = 0;         // reset the idle counter to prevent power safe shutdown

  sampleCounter = sampleCounter + 1;  // sample counter is not reset after start / stop sampling
  if (sampleCounter > (16777216) - 1) // sample counter wrapping at 2^24-1
  {
    helpers::printToLog("sample counter wrap at = " + String(sampleCounter), verboseLevels::info);
    sampleCounter = 0;
    sampleCounterOld = 0;
  }

  // convert sample counter to bytes  (32 bit, but only 24bits are send)
  bufferCounter.buffer[0] = (uint8_t)(sampleCounter)&0xFF; // LSB
  bufferCounter.buffer[1] = (uint8_t)(sampleCounter >> 8) & 0xFF;
  bufferCounter.buffer[2] = (uint8_t)(sampleCounter >> 16) & 0xFF;
  // bufferCounter.buffer[3]  = (uint8_t)(sampleCounter >> 24) & 0xFF;  //MSB   //disabled because only 24bits are used

  // convert current time to bytes  (32 bit)
  uint32_t currentTime = millis();
  if (currentTime > (millisWraps + 1) * ((16777216) - 1)) // wrapping at 2^24-1
  {
    helpers::printToLog("millis clock  wrap at = " + String(currentTime), verboseLevels::info);
    millisWraps = millisWraps + 1;
  }
  currentTime = currentTime - (millisWraps * ((16777216) - 1)); // subtract number of wraps times 2^24. Number of wraps start at zero.

  bufferMillis.buffer[0] = (uint8_t)(currentTime)&0xFF; // LSB
  bufferMillis.buffer[1] = (uint8_t)(currentTime >> 8) & 0xFF;
  bufferMillis.buffer[2] = (uint8_t)(currentTime >> 16) & 0xFF;
  // bufferMillis.buffer[3] = (uint8_t)(currentTime >> 24) & 0xFF;  //MSB //disabled because only 24bits are used

  bufferSampleSet.clearBuffer();                                                                                                      // clear single sample data buffer
  memcpy(bufferSampleSet.buffer, startBytes, startBytesSize);                                                                         // insert startByte
  memcpy(bufferSampleSet.buffer + startBytesSize, bufferCounter.buffer, bufferCounter.getBufferSize());                               // append sample counter
  memcpy(bufferSampleSet.buffer + startBytesSize + bufferCounter.getBufferSize(), bufferMillis.buffer, bufferMillis.getBufferSize()); // append millisecond counter data, starting pointer after the sample counter bytes
  bufferSensor.clearBuffer();                                                                                                         // clear Sensor read buffer

#if defined(PuppyESP_enabled)

  // TODO: does adc1_channel_7 match with ADC_CHANNEL_7
  // ADC_A0_VALUE = esp_adc_cal_get_voltage(ADC_CHANNEL_7, &adc_chars, &ADC_A0_VALUE); //Analog_channel_A0_pin

  static uint8_t batteryVoltage = 0;
  float batteryVoltFloat = 2900; // init with battery "full" value, will result in 5 Volt value on the status

  batteryVoltFloat = analogRead(batt_sw_in_GPIO); // read ADC via ESP32 calibrated method for ADC1

  batteryVoltage = (uint8_t)((batteryVoltFloat / 50.0) / battCalibrationFactor); // 2x since it is half the voltage, and /100 for mV to 1/10 V
  if (batteryVoltage <= battLowSleepVoltage)
  {
    lcd.setCursor(0, 0);
    lcd.print("Batt Empty: " + String(float(batteryVoltage) / 10.0) + " V   ");
    lcd.setCursor(0, 1);
    lcd.print("----------------------");
    lcd.setCursor(0, 2);
    lcd.print("Device will           ");
    lcd.setCursor(0, 3);
    lcd.print("power down now!         ");
    delay(2000);
    bool sleepNow = true;

    if (sleepNow)
    {
      aquisitionTask.disable();
      helpers::printToLog("Battery empty: disabled aquisitionTask (stop sampling) and going into sleep.", verboseLevels::error, PIN_LED_R);
      delay(1000);

      goIntoSleep();
    }
  }

  static bool waitSwitch = true;

  succesRead = bsecSupport.sampleSensorData(sensorBuffer, rawSensorBuffer, sensorStatusBuffer);
  sensorStatusBuffer[6] = (uint8_t)((batteryVoltage)&0xFF); // insert batteryVoltage
  // TODO: copy ranges
  for (int i = 0; i < sizeof(sensorBuffer); i++)
  {
    bufferSensor.buffer[i] = sensorBuffer[i];
  }
  for (int i = 0; i < sizeof(rawSensorBuffer); i++)
  {
    bufferSensorRaw.buffer[i] = rawSensorBuffer[i];
  }
  for (int i = 0; i < sizeof(sensorStatusBuffer); i++)
  {
    bufferSensorStatus.buffer[i] = sensorStatusBuffer[i];
  }


  lcd.setCursor(0, 0);
  if (bsecSupport.accuracyVOC == 0)
  {
    lcd.print("VOC: unstable         ");
  }
  else
  {
  lcd.print("VOC: " + String(float(bsecSupport.voc) / 100.0) + " ppm " + bsecSupport.accuracyLabel(bsecSupport.accuracyVOC)[0] + "       ");
  }
  lcd.setCursor(0, 1);
  if (bsecSupport.accuracyVOC == 0)
  {
    lcd.print("IAQ: unstable         ");
  }
  else
  {
  lcd.print("IAQ: " + String(float(bsecSupport.compIAQ) / 100.0) + " " + bsecSupport.accuracyLabel(bsecSupport.accuracyIAQ)[0] + "        ");
  }
  lcd.setCursor(0, 2);
  lcd.print(String(float(bsecSupport.humidity) / 100.0) + " % " + String(float(bsecSupport.temp) / 100.0) + " " + char(0xDF) + "C");

  lcd.setCursor(0, 3);
  if (batteryVoltage <= battLowWaringVoltage)
  {
    lcd.print("Batt Low: " + String(float(batteryVoltage) / 10.0) + " V     ");
  }
  else
  {
    lcd.print("Batt: " + String((float)batteryVoltage / 10.0) + " V " + battErrorDisp(batt_stat1_GPIO) + " " + battChargeDisp(batt_stat2_GPIO));
    // lcd.print("Gas: " + String(bsecSupport.gas_raw) + " Ohm   ");
  }



#endif // puppy

  // append Sensor data, starting pointer after the sample counter bytes
  memcpy(bufferSampleSet.buffer + startBytesSize + bufferCounter.getBufferSize() + bufferMillis.getBufferSize(), bufferSensor.buffer, bufferSensor.getBufferSize());

  memcpy(bufferSampleSet.buffer + startBytesSize + bufferCounter.getBufferSize() + bufferMillis.getBufferSize() + bufferSensor.getBufferSize(), bufferSensorRaw.buffer, bufferSensorRaw.getBufferSize());                                         // append Sensor Raw data
  memcpy(bufferSampleSet.buffer + startBytesSize + bufferCounter.getBufferSize() + bufferMillis.getBufferSize() + bufferSensor.getBufferSize() + bufferSensorRaw.getBufferSize(), bufferSensorStatus.buffer, bufferSensorStatus.getBufferSize()); // append Sensor Status data

  // calculate CRC:
  crc.add((uint8_t *)bufferSampleSet.buffer, (bufferSampleSet.getBufferSize() - 2)); // ommit the last two positions, since the CRC itself will be placed there.
  // copy CRC result into buffer
  bufferCRC.buffer[0] = (uint8_t)((crc.getCRC()) & 0xFF);                                                                                                                                                                                                            // LSB
  bufferCRC.buffer[1] = (uint8_t)((crc.getCRC() >> 8) & 0xFF);                                                                                                                                                                                                       // MSB
  crc.restart();                                                                                                                                                                                                                                                     // reset the CRC calculation for the next sample
  memcpy(bufferSampleSet.buffer + startBytesSize + bufferCounter.getBufferSize() + bufferMillis.getBufferSize() + bufferSensor.getBufferSize() + bufferSensorRaw.getBufferSize() + bufferSensorStatus.getBufferSize(), bufferCRC.buffer, bufferCRC.getBufferSize()); // append CRC data

  // the "bufferSampleSet" is done, all samples of a single sample set are present
  //  the code below will append this "bufferSampleSet" to one of the two btBuffers based upon their lock status. Before writing the btBuffer will be locked, and will not be available for sending. In this way race conditions are prevented.
  if (succesRead) // TODO: this only checks if the Sensor read was successfull, might be useful to check for ADC data as well.
  {
    if (btBufferA.isBufferLocked())
    {
      if (btBufferB.isBufferLocked())
      {
        helpers::printToLog("both write buffers locked!", verboseLevels::error, PIN_LED_R);
      }
      else
      {
        // buffer B not locked, write to buffer B
        memcpy(btBufferB.buffer + btBufferB.getPointerPosition(), bufferSampleSet.buffer, bufferSampleSet.getBufferSize()); // copy read buffer into send buffer
        bufferSampleSet.clearBuffer();                                                                                      // clear read buffer
        btBufferB.setPointerPosition(btBufferB.getPointerPosition() + bufferSampleSet.getBufferSize());                     // move pointer for next write to send buffer
        if (btBufferB.getPointerPosition() > btBufferB.getBufferSize() - bufferSampleSet.getBufferSize())                   // TODO: should this be checked before moving the pointer?
        {
          helpers::printToLog("Buffer B overflow!, dropped Buffer B", verboseLevels::error, PIN_LED_R);
          btBufferB.clearBuffer(); // clear buffer and reset pointer
        }
      }
    }
    else
    {
      // buffer A not locked, write to buffer A
      memcpy(btBufferA.buffer + btBufferA.getPointerPosition(), bufferSampleSet.buffer, bufferSampleSet.getBufferSize()); // copy read buffer into send buffer
      bufferSampleSet.clearBuffer();                                                                                      // clear read buffer
      btBufferA.setPointerPosition(btBufferA.getPointerPosition() + bufferSampleSet.getBufferSize());                     // move pointer for next write to send buffer
      if (btBufferA.getPointerPosition() > btBufferA.getBufferSize() - bufferSampleSet.getBufferSize())                   // TODO: should this be checked before moving the pointer?
      {
        helpers::printToLog("Buffer A overflow!, dropped Buffer A", verboseLevels::error, PIN_LED_R);
        btBufferA.clearBuffer(); // clear buffer and reset pointer
      }
    }
  }
  else
  {
    helpers::printToLog("Sensor read failed at sample counter: " + String(sampleCounter), verboseLevels::error, PIN_LED_R);
  }

  // log every n samples to show progress, toggle led when message is given
  static uint16_t logFreqSamples = 500;
  if (sampleCounter - sampleCounterOld > logFreqSamples - 1)
  {
    sampleCounterOld = sampleCounter;

    float millisDelta = millis() - millisOld;
    millisOld = millis();
    float rateHz = 1000 / (millisDelta / logFreqSamples);

    helpers::printToLog("sample counter: " + String(sampleCounter) + ", delta: " + (String)millisDelta + " ms, rate: " + (String)rateHz + " | ", verboseLevels::info);
    helpers::printToLog("BT bytes available for writing: " + String(btSupport.bluetoothAvailableForWrite()), verboseLevels::extraBT);

#ifdef BNO055_enabled
    toggleLED(PIN_LED_B); // blink the blue led on the IMU PCB
#endif                    // BNO055
#ifdef M5Stick_enabled
    M5.Lcd.setTextColor(0x07E0, 0x0000); // Green , black
    M5.Lcd.drawString("SampleCounter:", 0, 0, 4);
    M5.Lcd.drawString(String(sampleCounter), 0, 20, 4);

#endif // M5
  }

#if defined(UART_Force) || defined(MPU9250_SPI_enabled) // cadance is controlled by the Force ADC providing ~500 Hz Data stream
  // btSupport.bluetoothClearWriteError(); //clear errorse
  // communicationTask.forceNextIteration();
  aquisitionTask.forceNextIteration(); // schedule the aquisition task for the earliest possible execution

#endif // UART Force
}

// this task will check if one of the btBuffers contains sufficient data and is unlocked. It will then send it to the Bluetooth receiver.
// this task is executed at regular intervals, and is executed in a different thread as the aquisition task.
// this task will be active right from the boot, and will never be stopped.
void communicationTask_Callback()
{
  // if (helpers::getVerboseLevel() == verboseLevels::extraTiming)
  // {
  //   if (communicationTask.getOverrun() != communicationTaskDuration)
  //   {
  //     helpers::printToLog("overrun task 2 = " + String(communicationTask.getOverrun()) + ", task 2 start delayed by: " + String(communicationTask.getStartDelay()), verboseLevels::extraTiming);
  //   }
  // }
  idleCounter = idleCounter + 1; // counter is reset to zero in the aquisition loop
  if (idleCounter % 100 == 0)
  { // devidable by 100
    helpers::printToLog("Idle Counter: " + String(idleCounter) + " CommunicationTaskDuration: " + String(communicationTaskDuration) + " sleepTimeOut: " + String(sleepTimeOut), verboseLevels::extraSensor);
  }

  // button handler
  if (buttonS1.pressed == true && buttonS1.lastPressMillis + 2000 < millis()) // more than two seconds ago
  {
    buttonS1.pressed = false;
    buttonS1.lastPressMillis = millis(); // reset timer
    idleCounter = 0;                     // reset counter
    idleWarningGiven = false;            // reset idle warning, otherwise the message will only be shown once
    helpers::printToLog("Button S1 has been pressed.", verboseLevels::info);
    helpers::printToLog("Idle Counter: " + String(idleCounter) + " CommunicationTaskDuration: " + String(communicationTaskDuration) + " sleepTimeOut: " + String(sleepTimeOut), verboseLevels::extraSensor);
    delay(100); // some time if aquisition was recently started / stopped.
    if (aquisitionTask.isEnabled())
    {
      aquisitionTask.disable();
      helpers::printToLog("Manually disabled aquisitionTask (stop sampling)", verboseLevels::info);
#if defined(PuppyESP_enabled)
      digitalWrite(batt_sw_out_GPIO, LOW); // set batt_sw_out_GPIO to diable batt reading
      lcd.setCursor(0, 0);
      lcd.print("Idle              ");
      lcd.setCursor(0, 1);
      lcd.print("                 ");
      lcd.setCursor(0, 2);
      lcd.print("                 ");
#endif // puppy
    }
    else
    {

      jitterCounter = 0; // reset the counter for a clean start
      aquisitionTask.enable();
      helpers::printToLog("Manually enabled aquisitionTask (start sampling)", verboseLevels::info);
#if defined(PuppyESP_enabled)
      digitalWrite(batt_sw_out_GPIO, HIGH); // set batt_sw_out_GPIO high to enable batt reading
      lcd.setCursor(0, 0);
      lcd.print("Active              ");
#endif // puppy
    }
  }
  else
  {
    buttonS1.pressed = false;
  }

  if (idleCounter > sleepTimeOut * (1000 / communicationTaskDuration)) // if this task is idle too long (thus no sensor data is aquired) a power save shutdown is triggerd
  {
    if (!aquisitionTask.isEnabled())
    {
      helpers::printToLog("idle timeout, system will shutdown now!", verboseLevels::info);
      helpers::printToLog("Idle Counter: " + String(idleCounter) + " CommunicationTaskDuration: " + String(communicationTaskDuration) + " sleepTimeOut: " + String(sleepTimeOut), verboseLevels::extraSensor);
      goIntoSleep();
    }
    else
    {
      helpers::printToLog("idle timeout triggered, but aquisition is still running!", verboseLevels::error, PIN_LED_R);
    }
  }
  else if (idleCounter > sleepTimeOutWarning * (1000 / communicationTaskDuration) && !idleWarningGiven) // if this task is idle long (thus no sensor data is aquired) a power save warning is given
  {
    idleWarningGiven = true;
    if (aquisitionTask.isEnabled())
    {
      helpers::printToLog("idle warning triggered, but aquisition is still running!", verboseLevels::error, PIN_LED_R);
    }
    else
    {
      helpers::printToLog("idle warning, system will shutdown in one minute!", verboseLevels::info);
      helpers::printToLog("Idle Counter: " + String(idleCounter) + " CommunicationTaskDuration: " + String(communicationTaskDuration) + " sleepTimeOutWarning: " + String(sleepTimeOutWarning), verboseLevels::extraSensor);
      lcd.setCursor(0, 0);
      lcd.print("About to sleep              ");
      // TODO: make led blink?
    }
  }

  // read and display battery voltage when device is NOT sampling:
  if (!aquisitionTask.isEnabled() && idleCounter % 20 == 0) // devidable by 20)
  { 
    uint16_t batteryVoltage = 0;
    batteryVoltage = readBatteryVoltage(batt_sw_out_GPIO, batt_sw_in_GPIO, batt_stat1_GPIO, batt_stat2_GPIO, battCalibrationFactor);
    lcd.setCursor(0, 3); 
    lcd.print("Batt: " + String((float)batteryVoltage / 1000.0) + " V " + battErrorDisp(batt_stat1_GPIO) + " " + battChargeDisp(batt_stat2_GPIO));

  }

  if (btSupport.bluetoothIsAvailable())
  {
    // characters between the start and end marker are parsed as commands. For example "foo<start>foo"  is returned as "start"
    const char startMarker = '<';
    const char endMarker = '>';
    const int maxReadTime = 100; // to be implemented, not functional yet
    char *receivedChars = btSupport.bluetoothReceiveSerialMessage(startMarker, endMarker, maxReadTime);

    if (!btConnected) // was not yet connected
    {
      helpers::printToLog("Bluetooth connected!", verboseLevels::info);
      btConnected = true; // TODO: this remains true forever, even after disconnect
    }

    if (strcmp(receivedChars, "start") == 0)
    {
      jitterCounter = 0; // reset the counter for a clean start
      aquisitionTask.enable();
      helpers::printToLog("Start command received, enabled aquisitionTask (start sampling)", verboseLevels::info);

#if defined(PuppyESP_enabled)
      digitalWrite(batt_sw_out_GPIO, HIGH); // set batt_sw_out_GPIO high to enable batt reading
      lcd.setCursor(0, 0);
      lcd.print("Active             ");
#endif // puppy
    }
    else if (strcmp(receivedChars, "stop") == 0)
    {
      if (aquisitionTask.isEnabled())
      {
        aquisitionTask.disable();
        helpers::printToLog("Stop command received: disabled aquisitionTask (stop sampling).", verboseLevels::info);
#if defined(PuppyESP_enabled)
        digitalWrite(batt_sw_out_GPIO, LOW); // set batt_sw_out_GPIO low to disable batt reading
        lcd.setCursor(0, 0);
        lcd.print("Idle            ");
        lcd.setCursor(0, 1);
        lcd.print("                ");
        lcd.setCursor(0, 2);
        lcd.print("                ");
#endif // puppy
      }
    }
    else if (strcmp(receivedChars, "sleep") == 0)
    {
      if (aquisitionTask.isEnabled())
      {
        helpers::printToLog("Received sleep request, but aquisition is active!", verboseLevels::error);
      }
      else // system is idle
      {
        goIntoSleep();
      }
    }
    else if (strcmp(receivedChars, "battery") == 0)
    {
      eTaskState aquisitionTask_Status = eTaskGetState(&aquisitionTask);

      if (aquisitionTask_Status == eRunning) // aquisitionTaskActive.isEnabled())
      {
        helpers::printToLog("Received battery info request, but aquisition is active!", verboseLevels::error);
      }
      else // system is idle
      {

#if defined(M5Stick_enabled) || defined(ESP_Dev_UART) || defined(PuppyESP_enabled)

        uint8_t batteryVoltageBuffer[4];
        uint16_t batteryVoltage = 0;

#ifdef M5Stick_enabled
        batteryVoltage = M5.Axp.GetVbatData(); // read out battery voltage via I2C monitor chip inside M5, returns value in mV.

#endif // M5

#if defined(ESP_Dev_UART) || defined(PuppyESP_enabled)

        batteryVoltage = readBatteryVoltage(batt_sw_out_GPIO, batt_sw_in_GPIO, batt_stat1_GPIO, batt_stat2_GPIO, battCalibrationFactor);

#endif // ESP_Dev_UART

        // create start byte, different from normal samples
        // const unsigned char startByteA = 0xFF; //two bytes placed in front of every sample set
        const unsigned char startByteC = 0xFB; // two bytes placed in front of battery info
        batteryVoltageBuffer[0] = startByteA;
        batteryVoltageBuffer[1] = startByteC;
        batteryVoltageBuffer[2] = (uint8_t)((batteryVoltage)&0xFF);
        batteryVoltageBuffer[3] = (uint8_t)((batteryVoltage >> 8) & 0xFF);

        lcd.setCursor(0, 3);
        lcd.print("Batt: " + String((float)batteryVoltage / 1000.0) + " V      ");
        // write to BT five times
        for (size_t i = 0; i < 5; i++)
        {
          btSupport.bluetoothWrite((uint8_t *)batteryVoltageBuffer, sizeof(batteryVoltageBuffer)); // write buffer until pointer position
        }

#else
        helpers::printToLog("Battery readout not implemented on this platform!", verboseLevels::info);

#endif // M5 or ESP_Dev_UART
      }
    }
    else if (strncmp(receivedChars, "calibstate", 10) == 0) // command starting with "calibstate"
    {
      eTaskState aquisitionTask_Status = eTaskGetState(&aquisitionTask);

      if (aquisitionTask_Status == eRunning) // aquisitionTaskActive.isEnabled())
      {
        helpers::printToLog("Received calibstate command, but aquisition is active!", verboseLevels::error);
      }
      else // system is idle
      {
        // state is written to flash via EEPROM command, and loaded from the same flash

        // command structure: no spaces, no caps
        //
        // calibstate = start commmand
        // update h = argument (always 7 characters long, use padding)
        // end calibstate = end command
        //
        // supported arguments:
        // clear = earase flash memory
        // update h / m / l = write state to flash as soon as IAQ accuracy is high / medium / low
        // load = loads state if present in memory

        char *pointerStrEnd;
        pointerStrEnd = strstr(receivedChars, "endcalibstate");

        if (pointerStrEnd != NULL)
        {
          // TODO: check if correct number of characters are received between calibstate and end calibstate command
          char argument[8];                        // stores the value received from the host in ASCCI text
          memcpy(argument, &receivedChars[10], 7); // copy characters from full input command per channel
          argument[7] = '\0';                      // termination character

          helpers::printToLog("Received CalibState Command: " + String(argument), verboseLevels::info);
          if (strncmp(argument, "clear  ", 7) == 0) // check argument match
          {
            // Erase the EEPROM with zeroes

            for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE + 1; i++)
              EEPROM.write(i, 0);

            bool result = EEPROM.commit();
            if (result)
            {
              helpers::printToLog("BSEC clearing EEPROM.", verboseLevels::info);
            }
            else
            {
              helpers::printToLog("BSEC failed clearing EEPROM.", verboseLevels::error);
            }
          }
          else if (strncmp(argument, "load   ", 7) == 0) // check argument match
          {
            if (EEPROM.read(0) == BSEC_MAX_STATE_BLOB_SIZE) // calibration present
            {
              bool calibrationLoaded = bsecSupport.loadState();
              if (!calibrationLoaded)
              {
                helpers::printToLog("BME680 load calibration failed! ", verboseLevels::error, PIN_LED_R);
              }
            }
            else
            {
              helpers::printToLog("BME680 load calibration failed, no calib present in Flash!", verboseLevels::error, PIN_LED_R);
            }
          }
          else if (strncmp(argument, "updatel", 7) == 0) // check argument match
          {
            // update the calibration info on the device if accuracy of the IAQ value is "high"
            bool result = bsecSupport.updateState();
            if (result)
            {
              helpers::printToLog("BSEC update calibration at LOW accuracy.", verboseLevels::info);
            }
            else
            {
              helpers::printToLog("BSEC update calibration failed.", verboseLevels::error);
            }
          }
          else
          {
            helpers::printToLog("Received unknown calibstate argument!", verboseLevels::error);
          }
        }
        else
        {
          helpers::printToLog("Received incomplete calibstate command!", verboseLevels::error);
        }
        receivedChars = {0}; // clear input buffer
      }
    }
    else if (strcmp(receivedChars, "flush") == 0)
    {
      btSupport.bluetoothFlush();
    }
  }

  // write data to Bluetooth

  if (btBufferA.isBufferLocked() && btBufferB.isBufferLocked())
  {
    helpers::printToLog("at Send: both write buffers locked!", verboseLevels::error, PIN_LED_R);
  }

  // There are two data buffers: A & B, in the code below control is taken on which buffer is send and which is locked.
  // When Buffer A is filled with more data than one sample (SampleCounter + IMU), this buffer is locked, and send to Bluetooth by the code below.
  // The code where the IMU data is put into the buffer (at the higher sample rate) now switches to the other buffer because the code below has locked the first buffer.
  // The second time the code below is ran, it will send the second buffer, as such causing a ping-pong sending mechanism.
  // The task that collects the IMU data has higher prio than the Bluetooth sending data, allowing some jitter on the sending without hampering the 100 Hz IMU sample rate.
  // helpers::printToLog(" buffer A lock: " + String(btBufferA.isBufferLocked()) + " buffer B lock: " + String(btBufferB.isBufferLocked()) + ", buffer A position: " + String(btBufferA.getPointerPosition())+ ", buffer B position: " + String(btBufferB.getPointerPosition()), verboseLevels::extraBuffers);

  if (btBufferA.isBufferLocked())
  {
    if (btBufferB.getPointerPosition() > bufferSampleSet.getBufferSize()) // Buffer B is available for sending
    {
      btBufferB.lockBuffer();   // lock buffer B before sending it to the BT Receiver
      btBufferA.unlockBuffer(); // make buffer A available for writing
      helpers::printToLog(" try to send buffer B of size (bytes): " + String(btBufferB.getPointerPosition()) + ", data: " + String(btBufferB.buffer[10]), verboseLevels::extraBuffers);
      if (btSupport.bluetoothIsConnected())
      {
        btSupport.bluetoothWrite((uint8_t *)btBufferB.buffer, btBufferB.getPointerPosition()); // write buffer until pointer position
      }
      btBufferB.clearBuffer(); // clear buffer B & reset pointer
    }
  }
  else if (btBufferA.getPointerPosition() > bufferSampleSet.getBufferSize()) // Buffer A is available for sending
  {
    btBufferA.lockBuffer();   // lock buffer A before sending it to the BT Receiver
    btBufferB.unlockBuffer(); // make buffer B available for writing

    helpers::printToLog(" try to send buffer A of size (bytes): " + String(btBufferA.getPointerPosition()) + ", data: " + String(btBufferA.buffer[10]), verboseLevels::extraBuffers);
    if (btSupport.bluetoothIsConnected())
    {
      btSupport.bluetoothWrite((uint8_t *)btBufferA.buffer, btBufferA.getPointerPosition()); // write buffer until pointer position
    }
    btBufferA.clearBuffer(); // clear buffer A & reset pointer
  }
}

void setup(void)
{
  Serial.begin(115200); // initialize the USB serial console for debug messages

  setCpuFrequencyMhz(80); // default is 240 MHz, 80 MHz works, 40, 20 & 10 MHz fails to boot //https://deepbluembedded.com/esp32-change-cpu-speed-clock-frequency/
  delay(10);
  helpers::printToLog("CPU speed: " + (String)((uint32_t)getCpuFrequencyMhz()) + " MHz.", verboseLevels::info);

  btSupport.bluetoothInit(); // initialize the bluetooth class
  helpers::printToLog("Bluetooth init", verboseLevels::info);

  btSupport.bluetoothStart(); // begin Bluetooth Serial and set timeout
  helpers::printToLog("The device started, now you can pair it with bluetooth!", verboseLevels::info);
  helpers::printToLog("Version: " + (String)versionNumber + " Compiled at " + (String)__DATE__ + " " + (String)__TIME__, verboseLevels::info);

  delay(10);

  pinMode(PIN_LED_R, OUTPUT); // led of the M5 or Red led on the BNO055 PCB
  gpio_pulldown_en((gpio_num_t)PIN_LED_R);
  gpio_pullup_dis((gpio_num_t)PIN_LED_R);
  digitalWrite(PIN_LED_R, LOW); // init log

  // Configure CRC
  crc.setPolynome(0x1021);  // set polynome, note reset sets a default polynome.
  crc.setStartXOR(0xFFFF);  // set startmask, Initial value = 0xFFFF according to http://srecord.sourceforge.net/crc16-ccitt.html
  crc.setEndXOR(0x0000);    // set endmask, default 0.
  crc.setReverseIn(false);  // reverse the bitpattern of input data (MSB vs LSB).
  crc.setReverseOut(false); // reverse the bitpattern of CRC (MSB vs LSB).

  // initialize the buffers used to store data
  bufferCounter.initBuffer(3, 0);                                                                                                                                                                                                                 // counter buffer 24bit, 3x2 byte
  bufferMillis.initBuffer(3, 0);                                                                                                                                                                                                                  // millisecond timer counter 24bit, 3x2 byte
  bufferSensor.initBuffer(16, 0);                                                                                                                                                                                                                 // IMU sensor data buffer  16bit xyz accel,gyro,magneto, 3x3x2 byte
  bufferSensorRaw.initBuffer(16, 0);                                                                                                                                                                                                              // Force sensor data buffer    16bit 6 channels, 6x2 byte
  bufferSensorStatus.initBuffer(8, 0);                                                                                                                                                                                                            // uint8_t accuracyCO2, uint8_t accuracyVOC, uint8_t accuracyIAQ, uint8_t accuracyPercentGAS, uint8_t statusInitStab, uint8_t statusPowerOnStab, uint8_t batteryVoltage
  bufferCRC.initBuffer(2, 0);                                                                                                                                                                                                                     // CRC-16 data buffer, 2x2 byte
  bufferSampleSet.initBuffer(startBytesSize + bufferCounter.getBufferSize() + bufferMillis.getBufferSize() + bufferSensor.getBufferSize() + bufferSensorRaw.getBufferSize() + bufferSensorStatus.getBufferSize() + bufferCRC.getBufferSize(), 0); // complete buffer for a single sample set
  btBufferA.initBuffer(communicationBufferSize, buffers::bufferSampleSet.getBufferSize());                                                                                                                                                        // start pointer at sizeof(buffer) (otherwise you miss the first sample set)
  btBufferB.initBuffer(communicationBufferSize, buffers::bufferSampleSet.getBufferSize());                                                                                                                                                        // start pointer at sizeof(buffer) (otherwise you miss the first sample set)

  // Configure error LED Output //Led is off at the "HIGH" state
  // pinMode(PIN_LED, OUTPUT); //led on the firebeetle PCB is not used
  // digitalWrite(PIN_LED, LOW);

  // digitalWrite(PIN_LED_R, LOW);

  analogReadResolution(12);                                                                  // 12bit ADC
  analogSetWidth(12);                                                                        // 12bit ADC
                                                                                             // set ADC settings for calibration
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars); // TODO: this is limited to ADC1, but both ADCs are in use.

  // Increment boot number and print it every reboot
  ++bootCount;
  helpers::printToLog("Boot number: " + String(bootCount), verboseLevels::info);

  // Print the wakeup reason for ESP32
  print_wakeup_reason(esp_sleep_get_wakeup_cause());

#ifdef PuppyESP_enabled // BME gass sensor connected to I2C via CN3 header of the Puppy Nose

  // TODO: these are not functional since these GPIOs are input-only and do not have pullup/down :(

  // gpio_pullup_en(wakeUp_GPIO);  //enable pullup for power button
  // gpio_pulldown_dis(wakeUp_GPIO);
  // gpio_pullup_en(button_s2_GPIO);  //enable pullup for S2 button
  // gpio_pulldown_dis(button_s2_GPIO);
  // gpio_pullup_en(batt_stat1_GPIO);  //enable pullup for status line one of BQ25171 batt monitor
  // gpio_pulldown_dis(batt_stat1_GPIO);
  // gpio_pullup_en(batt_stat2_GPIO);  //enable pullup for status line two of BQ25171 batt monitor
  // gpio_pulldown_dis(batt_stat2_GPIO);

  // disable pullup,  PCB has pullup next to the I2C display.
  gpio_pulldown_dis(i2c_sda);
  gpio_pulldown_dis(i2c_scl);

  pinMode(batt_sw_in_GPIO, INPUT);                 // battery voltage readout
  pinMode(batt_sw_out_GPIO, OUTPUT);               // battery voltage readout enable / disable pin
  gpio_pullup_en((gpio_num_t)batt_sw_out_GPIO);    // enable pullup for batt voltage enable, R14 on PCB is the pulldown
  gpio_pulldown_dis((gpio_num_t)batt_sw_out_GPIO); // disable pulldown for batt voltage enable, R14 on PCB is the pulldown
  pinMode(button_s1_GPIO, INPUT);                  // S1 button, externall pullup
  pinMode(button_s2_GPIO, INPUT);                  // S2 button, externall pullup
  pinMode(batt_stat1_GPIO, INPUT );                      // status line one of BQ25171 batt monitor (patch external pull-up)
  pinMode(batt_stat2_GPIO, INPUT );                      // status line two of BQ25171 batt monitor (patch external pull-up)

  // configure the wake up source to wake up for an external trigger.
  esp_sleep_enable_ext0_wakeup(button_s2_GPIO, 0); // 1 = High, 0 = Low

  attachInterrupt(buttonS1.PIN, button_S1_ISR, FALLING); // TODO: keep getting interupts from S2 when the button is NOT pressed. During press it is silent.  //change + falling + rising + high = continious events, silent when button is pressed - LOW = no events, no reaction

  uint16_t batteryVoltage = 0;
  batteryVoltage = readBatteryVoltage(batt_sw_out_GPIO, batt_sw_in_GPIO, batt_stat1_GPIO, batt_stat2_GPIO, battCalibrationFactor);
  const uint32_t i2c_freq = 100000;
  PUP_I2C.begin(i2c_sda, i2c_scl, i2c_freq);
  PUP_I2C.beginTransmission(LCD_i2caddr); // Check for LCD here
  if (PUP_I2C.endTransmission() == 0)
  {
    helpers::printToLog("LCD device found ", verboseLevels::info);

    lcd.begin(16, 4); // https://www.lcd-module.com/fileadmin/eng/pdf/doma/dogs164e.pdf
    initPuppyLCD(PUP_I2C, i2c_sda, i2c_scl, i2c_freq, LCD_i2caddr, PIN_LED_R);
  }
  else
  {
    helpers::printToLog("No response from LCD device!", verboseLevels::error, PIN_LED_R);
  }

  lcd.setCursor(0, 0);
  lcd.print("PuppyNose v" + (String)versionNumber);

  lcd.setCursor(0, 1);
  lcd.print("Bt:" + deviceName);
  // lcd.setCursor(0, 3);
  // lcd.print("Batt: " + String((float)batteryVoltage / 1000.0) + " V      ");

#endif //# Puppy

#ifdef BME680_I2C_enabled // BME gass sensor connected to I2C via CN3 header of the Puppy Nose
                          // initialize EEPROM with predefined size
  EEPROM.begin(EEPROM_SIZE);
  // const uint32_t i2c_freq = 100000;
  PUP_I2C.begin(i2c_sda, i2c_scl, i2c_freq);
  // PUP_I2C.begin(i2c_sda, i2c_scl);               // init I2C
  PUP_I2C.beginTransmission(BME680_I2C_address); // Check for BME680 here
  if (PUP_I2C.endTransmission() == 0)
  {
    if (bsecSupport.initBsecSensor(BME680_I2C_address, &PUP_I2C))
    {
      helpers::printToLog("BME680 device found, BSEC version: " + bsecSupport.getBsecVersion(), verboseLevels::info);
      lcd.setCursor(0, 2);
      lcd.print("BME680 found");
      helpers::printToLog("BME680 config: " + bsec_config_string, verboseLevels::info);
      bsecSupport.setBsecConfiguration(bsec_config_iaq);
      bsecSupport.loadState();
    }
    else
    {
      helpers::printToLog("BME680 device found, but initialization failed! ", verboseLevels::error, PIN_LED_R);
      lcd.setCursor(0, 2);
      lcd.print("BME680 failed!");
    }
  }
  else
  {
    helpers::printToLog("BME680 device missing! ", verboseLevels::error, PIN_LED_R);
    lcd.setCursor(0, 2);
    lcd.print("BME680 missing!");
  }

#endif //# BME680_I2C_enabled

  // start the scheduler worker. Tasks can be added the scheduler and given a certain execution frequency.
  runner.init();

  runner.addTask(aquisitionTask);
  helpers::printToLog("prepaired aquisition task. Waiting for 0.5 seconds.", verboseLevels::info);
  delay(500);

  runner.addTask(communicationTask);
  helpers::printToLog("prepaired communication task. Waiting for 0.5 seconds.", verboseLevels::info);
  delay(500);

  communicationTask.setSchedulingOption(TASK_SCHEDULE_NC); // disable catch-ups
  aquisitionTask.setSchedulingOption(TASK_SCHEDULE_NC);    // disable catch-ups

  communicationTask.enable();
  helpers::printToLog("Communication task started!", verboseLevels::info);

  // digitalWrite(PIN_LED_R, HIGH); // finish init, turn off red led to show all is OK
}

/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete
*/
/**************************************************************************/
void loop(void)
{

  // trigger the task scheduler, all other actions belong to one of the two tasks.
  runner.execute();
}