#include <BufferSupport.h>

#include <InstrumentedObjectHelpers.h>

using namespace helpers;

//constructor
BufferSupport::BufferSupport(){

};

//initialize a buffer with size and pointer position, default point position is 0.
bool BufferSupport::initBufferParent(int bufferSize, int initialPointerPosition = 0)
{
    _bufferSize = bufferSize;
    
 
    //unsigned char _buffer[bufferSize];   //TODO: buffer is now fixed to size of 10420, due to issues with dynamic allocation of c-type arrays. I have to look for a nicer solution.

    _bufferLocked = false;
    _pointerPosition = initialPointerPosition;
    helpers::printToLog("Init buffer with size: " + String(_bufferSize) + " and position: " + String(_pointerPosition), verboseLevels::extraBuffers);

    //condition checking
    if (_pointerPosition < 0)
    {
        helpers::printToLog("You try to create buffer with negative pointer position! pointerPosition is now 0!", verboseLevels::error);
        _pointerPosition = 0;        
    }
    else if (_pointerPosition > _bufferSize)
    {
        helpers::printToLog("You try to create buffer with pointer position beyond buffer size! pointerPosition is now equal to buffer size!", verboseLevels::error);
        _pointerPosition = _bufferSize;
    }
    else if (_bufferSize < 1 )
    {
        helpers::printToLog("You try to create buffer with 0 or negative size!", verboseLevels::error);
        return false;
    }
    
    return true;
}

// initialize a buffer with size and pointer position, default point position is 0.
bool LargeBufferSupport::initBuffer(int bufferSize, int initialPointerPosition = 0)
{
    bool success = true;

    success = initBufferParent(bufferSize, initialPointerPosition);

    if (_bufferSize > _maxArraySize)
    {
        // size_t n = ;
        helpers::printToLog("You try to create buffer with size larger than allocaled size!", verboseLevels::error);
        return false;
    }

    return success;
}

// initialize a buffer with size and pointer position, default point position is 0.
bool SmallBufferSupport::initBuffer(int bufferSize, int initialPointerPosition = 0)
{
    bool success = true;

    success = initBufferParent(bufferSize, initialPointerPosition);

    if (_bufferSize > _maxArraySize)
    {
        // size_t n = ;
        helpers::printToLog("You try to create buffer with size larger than allocaled size!", verboseLevels::error);
        return false;
    }

    return success;
}

//clear buffer writes zeros to all buffer positions and sets pointer position to 0.
void LargeBufferSupport::clearBuffer()
{
    //helpers::printToLog("Clear buffer with size: " + String(_bufferSize) +  " sizeOf gives: " + String(sizeof(buffer)) + " pointer before clear: " + String(_pointerPosition), verboseLevels::extraBuffers);

    memset(&buffer, 0, _bufferSize); //only need to clear the part that is in use _bufferSize
    _pointerPosition = 0;
}
//clear buffer writes zeros to all buffer positions and sets pointer position to 0.
void SmallBufferSupport::clearBuffer()
{
    //helpers::printToLog("Clear buffer with size: " + String(_bufferSize) +  " sizeOf gives: " + String(sizeof(buffer)) + " pointer before clear: " + String(_pointerPosition), verboseLevels::extraBuffers);

    memset(&buffer, 0, _bufferSize); //only need to clear the part that is in use _bufferSize
    _pointerPosition = 0;
}

//Buffer locking:  methods should check if the buffer is unlocked, then lock the buffer before writing, and unlock after writing.
//sets the lock flag, has no influence on buffer properties, buffer is still writable after locking!
void BufferSupport::lockBuffer()
{
    _bufferLocked = true;
}

//clears the lock flag:
void BufferSupport::unlockBuffer()
{
    _bufferLocked = false;
}

//getters
bool BufferSupport::isBufferLocked()
{
    return _bufferLocked;
}

int BufferSupport::getPointerPosition()
{
    return _pointerPosition;
}

int BufferSupport::getBufferSize()
{
    //helpers::printToLog("Returned buffer size: " + String(_bufferSize), verboseLevels::extraBuffers);
    return _bufferSize; //this is the intended buffer size, not the reserved memory! More memory is reserved than needed.
}

//setters
bool BufferSupport::setPointerPosition(int pointerPosition)
{
    _pointerPosition = pointerPosition;
    return true;
}