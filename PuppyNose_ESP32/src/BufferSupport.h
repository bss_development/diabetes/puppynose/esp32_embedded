#ifndef BUFFERSUPPORT_H
#define BUFFERSUPPORT_H

const int maxArraySize = 10240;  // Max allocation size
const int defaultArraySize = 50; // Default allocation size
// TODO: this is currently the allocation size for all buffers, waisting memory. Since all actions are performed on the initiated buffersize variable, it does not waste performance, it only leaves memory unused.

class BufferSupport
{

public:
    // constructor    
    BufferSupport();

    // methods
    bool initBufferParent(int bufferSize, int initialPointerPosition);
    void lockBuffer();
    void unlockBuffer();
    void clearBuffer();

    // getters
    bool isBufferLocked();
    int getPointerPosition();
    int getBufferSize();

    // setters
    bool setPointerPosition(int pointerPosition);



    // private:
    int _bufferSize;
    bool _bufferLocked;
    int _pointerPosition;
};

class SmallBufferSupport : public BufferSupport
{
public:
    // method:
    bool initBuffer(int bufferSize, int initialPointerPosition);
    void clearBuffer();

    // variable:
    unsigned char buffer[defaultArraySize]; // TODO: I do not want to allocate a fixed size here, but c-type arrays are not very dynamic :(

private:
    const int _maxArraySize = defaultArraySize;
};

class LargeBufferSupport : public BufferSupport
{
public:
    // method:
    bool initBuffer(int bufferSize, int initialPointerPosition);
    void clearBuffer();

    // variable
    unsigned char buffer[maxArraySize]; // TODO: I do not want to allocate a fixed size here, but c-type arrays are not very dynamic :(

private:
    const int _maxArraySize = maxArraySize;
};

#endif // BUFFERSUPPORT_H