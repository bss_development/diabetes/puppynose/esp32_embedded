
#include <InstrumentedObjectHelpers.h>




const int verboseLevel = helpers::verboseLevels::info; //define what output should be send to the USB serial console.




namespace helpers
{
  //returns the current static verbose level, all messages with an higher or equal level will be printed to the log
  extern const int getVerboseLevel()
  {
    return verboseLevel;
  }
  
  //converts to a string "> INFO: " etc.
  extern String verboseLevelToString(int verboseLevelToConvert)
  {
    switch (verboseLevelToConvert)
    {
    case verboseLevels::error:
      return "> ERROR: ";
    case verboseLevels::info:
      return "> INFO: ";
    case verboseLevels::extraBT:
      return "> EXTRA BT: ";
    case verboseLevels::extraSensor:
      return "> EXTRA SENSOR: ";
    case verboseLevels::extraTiming:
      return "> EXTRA TIMING: ";
    case verboseLevels::extraBuffers:
      return "> EXTRA TIMING: ";

    default:
      return "> (unknown level): ";
    }
  }

  //if the set verbose level is equal or higher than the verbose level of the individual message, it will be printed to the USB serial console
  extern void printToLog(String messsage, int verboseLevelOfThisMessage)
  {
    String verboseLevelStr = verboseLevelToString(verboseLevelOfThisMessage);
    if (getVerboseLevel() >= verboseLevelOfThisMessage)
    {
      Serial.print(millis());        //log current time
      Serial.print(verboseLevelStr); // "> ERROR: " or "> INFO: "
      Serial.println(messsage);      //write out the message
    }   
  }
  //if the set verbose level is equal or higher than the verbose level of the individual message, it will be printed to the USB serial console
  //overloaded version that enables the error led upon error. If LED is not available, set errorLedGPIO to -1 to suppress the error led
  extern void printToLog(String messsage, int verboseLevelOfThisMessage, int errorLedGPIO)
  {
    printToLog(messsage, verboseLevelOfThisMessage);
    if (verboseLevelOfThisMessage == verboseLevels::error) //error message, turn on error led if avaiable
    {
      if (errorLedGPIO >= 0) //led is indicated, if LED is not available, set it to -1 to suppress the error led
      {                                  
        digitalWrite(errorLedGPIO, HIGH); //show error
      }
    }
  }

  // Method to print the reason by which ESP32 has been awaken from sleep
  void print_wakeup_reason(esp_sleep_wakeup_cause_t wakeup_reason)
  {
    switch (wakeup_reason)
    {
    case ESP_SLEEP_WAKEUP_EXT0:

      helpers::printToLog("Wakeup caused by external signal using RTC_IO", verboseLevels::info);

      break;
    case ESP_SLEEP_WAKEUP_EXT1:
      helpers::printToLog("Wakeup caused by external signal using RTC_CNTL ", verboseLevels::info);
      break;
    case ESP_SLEEP_WAKEUP_TIMER:
      helpers::printToLog("Wakeup caused by timer ", verboseLevels::info);
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
      helpers::printToLog("Wakeup caused by touchpad", verboseLevels::info);
      break;
    case ESP_SLEEP_WAKEUP_ULP:
      helpers::printToLog("Wakeup caused by ULP program ", verboseLevels::info);
      break;
    default:
      helpers::printToLog("Wakeup was not caused by deep sleep:" + String(wakeup_reason), verboseLevels::info);
      break;
    }
  }

  //function to read battery voltage, is blocking for 150 ms
uint16_t readBatteryVoltage(uint8_t batt_sw_out_GPIO, uint8_t batt_sw_in_GPIO, gpio_num_t batt_stat1_GPIO, gpio_num_t batt_stat2_GPIO, float battCalibrationFactor)
{
   // status line one of BQ25171 batt monitor (enable interal pull-up)  
    // batt_stat1_GPIO: 0 = error, 1 = ok
    // batt_stat2_GPIO  0 = charging, 1 = idle
    String battErrorStatus = "";
    String battChargeStatus = "";    
    if(!digitalRead(batt_stat1_GPIO))
    {
      battErrorStatus = "Error in battery";
    }
    else
    {
      battErrorStatus = "";
    } 
    
    if(!digitalRead(batt_stat2_GPIO))
    {
      battChargeStatus = "Charging";
    }
    else
    {
      battChargeStatus = "";
    }

        uint16_t batteryVoltage = 0;
        digitalWrite(batt_sw_out_GPIO, HIGH); // set batt_sw_out_GPIO high to enable batt reading
        delay(100);                           // wait for voltage to stabalize

        uint32_t batteryVoltage32 = 0;

        // readout value 5 times
        for (int i = 0; i < 5; i++)
        {
          batteryVoltage32 = batteryVoltage32 + analogRead(batt_sw_in_GPIO); // read ADC
          delay(10);
        }

        // calculate average
        batteryVoltage = (uint16_t)((2 * ((float)batteryVoltage32 / (float)5)) / battCalibrationFactor);

        // set batt_sw_out_GPIO low to disable batt reading
        digitalWrite(batt_sw_out_GPIO, LOW);

        helpers::printToLog("Battery voltage (mV)): " + String(batteryVoltage) +  " " + battChargeStatus + " " + battErrorStatus, verboseLevels::info);

        return batteryVoltage;
}

String battErrorDisp(gpio_num_t batt_stat1_GPIO)
{
  String battErrorDisp = " ";
    
    if(!digitalRead(batt_stat1_GPIO))
    {
      battErrorDisp = "E";
    }    
    return battErrorDisp;

}

String battChargeDisp(gpio_num_t batt_stat2_GPIO){
    String battChargeDisp = " ";    
   
    if(!digitalRead(batt_stat2_GPIO))
    {
      battChargeDisp = "C";
    }
    return battChargeDisp;
}
    

bool initPuppyLCD(TwoWire &i2cPort, gpio_num_t i2c_sda, gpio_num_t i2c_scl, uint32_t i2c_freq, uint8_t LCD_i2caddr, uint8_t errorLedGPIO)
{
  bool success = true;
  TwoWire *_i2cPort = &i2cPort;
  _i2cPort->begin(i2c_sda, i2c_scl, i2c_freq);        // init I2C
  _i2cPort->beginTransmission(LCD_i2caddr); // Check for LCD here
  if (_i2cPort->endTransmission() == 0)
  {
    // helpers::printToLog("LCD device found ", verboseLevels::info);

    //lcd.begin(16, 4); // https://www.lcd-module.com/fileadmin/eng/pdf/doma/dogs164e.pdf

    _i2cPort->beginTransmission(LCD_i2caddr);

    // Commands to initiate display by Richard Rasker
          // RE = 1  |  IS = 0
    _i2cPort->write(0x00); // Initiate Command Stream
    _i2cPort->write(0x3A); // Function set : 8bit 4line RE = 0, IS = 0
    _i2cPort->write(0x1F); // Double Height & Bias setting
    _i2cPort->write(0x09); // Extended function set
    _i2cPort->write(0x06); // Entry mode set
      // RE = 0  |  IS = 1
    _i2cPort->write(0x39); // Function set
    _i2cPort->write(0x1B); // Internal OSC
    _i2cPort->write(0x6C); // Follower control
    _i2cPort->write(0x54); // Power Control
    _i2cPort->write(0x78); // Contrast Set
       // RE = 0  |  IS = 0
    _i2cPort->write(0x38); // Function set, 4 display lines
    _i2cPort->write(0x0C); // Display On/Off: Display on, cursor off, blink off
    _i2cPort->write(0x01); // Clear display

    _i2cPort->endTransmission();
  }
  else
  {
    helpers::printToLog("No response from LCD device!", verboseLevels::error, errorLedGPIO);
    success = false;
  }

  return success;
}



}



// // readBytes is used to read multiple successive bytes from any I2C device, written to read the data from MPU6886, since its API only provided single byte readout
// bool readBytes(byte *buffer, uint8_t address, uint8_t deviceAddress, TwoWire &i2cPort, uint8_t length)
// {
//   TwoWire *_i2cPort = &i2cPort;
//   _i2cPort->beginTransmission(deviceAddress);
//   _i2cPort->write(address);
//   _i2cPort->endTransmission();
//   _i2cPort->requestFrom(deviceAddress, length);

//   for (uint8_t i = 0; i < length; i++)
//   {
//     buffer[i] = _i2cPort->read();
//   }

//   return true; // TODO: error checking
// }

// flip byte pairs is used to change from MSB first to LSB first in 16bit values
// input:  1 2 3 4 5 6   output: 2 1 4 3 6 5
// bool flipBytePairs(byte buffer[], byte bufferFlipped[], int length)
// {
//   // check if length is even
//   if (length % 2 != 0) // odd: error
//   {
//     memset(bufferFlipped, 0, length); // fill array with 0 to prevent random results.
//     helpers::printToLog("flipBytePairs: Cannot swap bytes if length is odd!", verboseLevels::error, PIN_LED_R);
//     return false;
//   }
//   else // length is even: OK
//   {
//     for (uint8_t i = 0; i < length; i++)
//     {
//       if (i % 2 == 0) // is even.
//       {
//         int16_t buffer16 = (int16_t)((buffer[i] << 8) | buffer[i + 1]); // reversed byte order such as used by MPU6886
//         bufferFlipped[i] = (uint8_t)(buffer16)&0xFF;                    // LSB
//         bufferFlipped[i + 1] = (uint8_t)(buffer16 >> 8) & 0xFF;         // MSB
//         // debug:
//         //  int16_t buffer16flipped = ((int16_t)bufferFlipped[i]) | (((int16_t)bufferFlipped[i+1]) << 8);  //convert back to signed int for display
//         //  helpers::printToLog("flipBytePairs index = "+ String(i) + " org = " + String(buffer16) + " flipped = " + String(buffer16flipped), verboseLevels::info);
//       }
//     }
//     return true;
//   }
// }
