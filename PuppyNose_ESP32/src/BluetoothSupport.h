#ifndef BLUETOOTHSUPPORT_H
#define BLUETOOTHSUPPORT_H

#include <BluetoothSerial.h>

class BluetoothSupport
{

public:
    //constructor
    BluetoothSupport(int timeOut, String deviceName);

    bool btConnected;
    BluetoothSerial SerialBT;
    char *receiveSerialMessage();
    bool bluetoothInit();
    bool bluetoothStart();
    bool bluetoothFlush();
    bool bluetoothDisconnect();
    bool bluetoothIsAvailable();
    bool bluetoothIsConnected();
    int bluetoothAvailableForWrite();
    char *bluetoothReceiveSerialMessage(const char startMarker, const char endMarker, const int maxReadTime);
    size_t bluetoothWrite(uint8_t *buffer, uint16_t bufferposition );

    //getters 
    uint8_t timeOut() const {return _timeOut;}
    String deviceName() const {return _deviceName;}

private:
    //drop connection after this time in ms
    uint8_t _timeOut;

    //public name of BT connection that shows in pairing list. Avoid duplicates!
    String _deviceName;
};

#endif // BLUETOOTHSUPPORT_H