#include <BluetoothSupport.h>

#include <InstrumentedObjectHelpers.h>
using namespace helpers;

//constructor
BluetoothSupport::BluetoothSupport(int timeOut, String deviceName)
{
    _timeOut = timeOut;
    _deviceName = deviceName;
};

const byte numChars = 32; //maximum length of messages parsed.

//initialize method:
bool BluetoothSupport::bluetoothInit()
{
    BluetoothSerial SerialBT; //serial interface to Bluetooth SPP
    return true;
}

//calls begin() on the device name and sets the timeout.
bool BluetoothSupport::bluetoothStart()
{

    SerialBT.begin(_deviceName);   //Start Bluetooth device name
    SerialBT.setTimeout(_timeOut); //set read timeout to 100ms
    return true;
}

//returns bytes waiting to be send
int BluetoothSupport::bluetoothAvailableForWrite()
{
    int available = SerialBT.availableForWrite();
    return available;
}

//calls flush() on the serial device
bool BluetoothSupport::bluetoothFlush()
{
    bool success = true;
    helpers::printToLog("Executed bt flush request!", verboseLevels::info);
    SerialBT.flush();           //wipe buffers
    SerialBT.clearWriteError(); //clear errors
    return success;
}

//calls disconnect() on the serial device
bool BluetoothSupport::bluetoothDisconnect()
{
    bool success = SerialBT.disconnect();
    return success;
}

//calls available() on the serial device, true if data is received
bool BluetoothSupport::bluetoothIsAvailable()
{
    bool success = SerialBT.available();
    return success;
}

//calls available() on the serial device, true if data is received
bool BluetoothSupport::bluetoothIsConnected()
{
    bool success = SerialBT.connected();
    return success;
}

//read messages received, this call is blocking for as long as new data continues to arrive
//default start and end markers are < & >, other can be defines via arguments.
//characters between the start and end marker are parsed as commands. For example "foo<start>foo"  is returned as "start"
//maxReadTime is reserved for future use, to implement a timeout function if new characters keep comming in.
//based on https://forum.arduino.cc/t/serial-input-basics-updated/382007
char *BluetoothSupport::bluetoothReceiveSerialMessage(const char startMarker = '<', const char endMarker = '>', const int maxReadTime = 100)
{
    static char receivedChars[numChars]; // an array to store the received data

    static boolean recvInProgress = false;
    boolean newData = false;
    static byte index = 0;
    char charactersRead;

    //TODO: cancel while loop if new data continues to arrive
    while (SerialBT.available() > 0 && newData == false)
    {
        charactersRead = SerialBT.read();

        if (recvInProgress == true)
        {
            if (charactersRead != endMarker)
            {
                receivedChars[index] = charactersRead;
                index++;
                if (index >= numChars)
                {
                    index = numChars - 1; //TODO: comment FM: does not seem logical to do?

                    helpers::printToLog("Received more characters via BT than the max array length!  " + String(receivedChars), verboseLevels::error);
                }
            }
            else
            {
                receivedChars[index] = '\0'; // terminate the string
                recvInProgress = false;
                index = 0;
                newData = true;
            }
        }

        else if (charactersRead == startMarker)
        {
            recvInProgress = true;
        }
    }
    if (newData == true) //print the parsed command for debug purposes.
    {
        helpers::printToLog("Received via BT: " + String(receivedChars), verboseLevels::info);
    }

    return receivedChars;
}

//write binary data to the Bluetooth serial interface
size_t BluetoothSupport::bluetoothWrite(uint8_t *buffer, uint16_t bufferposition)
{
    size_t btWriteOutput = SerialBT.write(buffer, bufferposition); //write buffer until pointer position
    helpers::printToLog(" Written to BT: " + String(btWriteOutput) + "> buffer position: " + String(bufferposition) + ", startFrame: " + String(buffer[bufferposition - 40]) + ", " + String(buffer[bufferposition - 39]), verboseLevels::extraBuffers);
    return btWriteOutput;
}
