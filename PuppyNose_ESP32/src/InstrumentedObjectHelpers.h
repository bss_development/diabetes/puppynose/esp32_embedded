//this include file contains constants used in the code for the instrumented object.

#ifndef INSTRUMENTEDOBJECTHELPERS_H   //TODO: #pragma once might be better
#define INSTRUMENTEDOBJECTHELPERS_H  

#include <WString.h>
#include <Arduino.h>
#include <Wire.h> 
//#include <SSD1803a_I2C.h> //https://github.com/firexx/SSD1803a_I2C  

namespace helpers
{
  enum verboseLevels
  {
    silent = 0,  // print nothing in the custom code. External libraries will still output debug info.
    error = 1,   // print all error messages
    info = 2,    // print all error and info messages
    extraBT = 3, //3 and above: print messages only used during development (can cause performance issues!)
    extraSensor = 4,
    extraTiming = 5,
    extraBuffers = 6
  };

  extern const int getVerboseLevel();

  extern String verboseLevelToString(int verboseLevelToConvert);


  //check the verboseLevels, add time and print to USB "Serial" terminal
  extern void printToLog(String messsage, int verboseLevelOfThisMessage);
  extern void printToLog(String messsage, int verboseLevelOfThisMessage, int errorLedGPIO);  //when defining the error LED GPIO, it will turn on when a message with level error is given
  

  void print_wakeup_reason(esp_sleep_wakeup_cause_t wakeup_reason);
  uint16_t readBatteryVoltage(uint8_t batt_sw_out_GPIO, uint8_t batt_sw_in_GPIO, gpio_num_t batt_stat1_GPIO, gpio_num_t batt_stat2_GPIO, float battCalibrationFactor);
  bool initPuppyLCD(TwoWire &i2cPort, gpio_num_t i2c_sda, gpio_num_t i2c_scl, uint32_t i2c_freq, uint8_t LCD_i2caddr, uint8_t errorLedGPIO);
  String battErrorDisp(gpio_num_t batt_stat1_GPIO);
  String battChargeDisp(gpio_num_t batt_stat2_GPIO);
}


#endif //INSTRUMENTEDOBJECTHELPERS_H