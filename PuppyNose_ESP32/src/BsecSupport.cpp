#include <BsecSupport.h>
#include "bsec.h" // Include the BME680 Sensor library
#include <InstrumentedObjectHelpers.h>
#include <EEPROM.h>

using namespace helpers;

#define STATE_SAVE_PERIOD UINT32_C(360 * 60 * 1000) // 360 minutes - 4 times a day

// Create an object of the class Bsec
Bsec iaqSensor;
uint8_t bsecState[BSEC_MAX_STATE_BLOB_SIZE] = {0};
uint16_t stateUpdateCounter = 0;
bool calibrationLoaded = false; //if calibration data is loaded from eeprom successfully, this bool is set, and no new attempt is made to load calibration info


String output;

// constructor
BsecSupport::BsecSupport(int PIN_LED_R)
{
  PIN_LED_R = _PIN_LED_R;
};

bool BsecSupport::initBsecSensor(const uint8_t I2C_address, TwoWire *i2cPort)
{

  // TwoWire *_i2cPort = &i2cPort;

  //_i2cPort->begin
  iaqSensor.begin(I2C_address, *i2cPort);

  voc = 0;      // breath VOC concentration estimate [ppm] *100
  humidity = 0; // compensated, in procent *100
  temp = 0;     // compensated, deg C *100

  compIAQ = 0; // Indoor-air-quality estimate [0-500]

  staticIAQ = 0; /*!< Unscaled indoor-air-quality estimate */

  co2 = 0; /*!< co2 equivalent estimate [ppm] */

  accuracyIAQ = 0;        // 0 = Stabilization / run-in ongoing, 1=low, 3=high
  accuracyVOC = 0;        // 0 = Stabilization / run-in ongoing, 1=low, 3=high
  accuracyCO2 = 0;        // 0 = Stabilization / run-in ongoing, 1=low, 3=high
  accuracyCompGAS = 0;    // 0 = Stabilization / run-in ongoing, 1=low, 3=high
  accuracyPercentGAS = 0; // 0 = Stabilization / run-in ongoing, 1=low, 3=high
  accuracyCombined = 0;   // combined, two bits per accuracy value.  order: IAQ, VOC, CO2, Percent Gas

  statusInitStab = 0;    // gas sensor element: initial stabilization  is ongoing (0) or finished (1).
  statusPowerOnStab = 0; // gas sensor element: power-on stabilization  is ongoing (0) or finished (1).
  statusCombined = 0;    // combined, one bit per status value.  order: Init, PowerOn

  compGas = 0;    /*!< Reserved internal debug output */
  percentGas = 0; /*!< percentage of min and max filtered gas value [%] */

  gas_raw = 0;      // uint32, Gas resistance measured directly by the BME680 in Ohm
  humidity_raw = 0; // uint32, Relative humidity directly measured by the BME680 in procent *1000
  pressure_raw = 0; // uint32, Pressure directly measured by the BME680 in Pa
  temp_raw = 0;     // int16, Temperature directly measured by BME680 in deg C *100

  return checkIaqSensorStatus();
}

bool BsecSupport::sampleSensorData(byte sensorBuffer[], byte rawSensorBuffer[], byte sensorStatusBuffer[])
{
  bool succesRead = true;

  if (iaqSensor.run())
  { // If new data is available

    accuracyIAQ = uint8_t(iaqSensor.iaqAccuracy);                   // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    accuracyVOC = uint8_t(iaqSensor.breathVocAccuracy);             // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    accuracyCO2 = uint8_t(iaqSensor.co2Accuracy);                   // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    accuracyCompGAS = uint8_t(iaqSensor.compGasAccuracy);           // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    accuracyPercentGAS = uint8_t(iaqSensor.gasPercentageAcccuracy); // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    statusInitStab = uint8_t(iaqSensor.stabStatus);                 // gas sensor element: initial stabilization  is ongoing (0) or finished (1).
    statusPowerOnStab = uint8_t(iaqSensor.runInStatus);             // gas sensor element: power-on stabilization  is ongoing (0) or finished (1).

    if (accuracyIAQ != 0)
    {
      compIAQ = uint16_t(iaqSensor.iaq * 100.0);         // Indoor-air-quality estimate [0-500]
      staticIAQ = uint16_t(iaqSensor.staticIaq * 100.0); /*!< Unscaled indoor-air-quality estimate */
    }

    if (accuracyVOC != 0)
    {
      voc = uint32_t(iaqSensor.breathVocEquivalent * 100.0); // breath VOC concentration estimate [ppm] *100
    }

    if (accuracyCO2 != 0)
    {
      co2 = uint32_t(iaqSensor.co2Equivalent * 100.0); /*!< co2 equivalent estimate [ppm] * 100 */
    }

    humidity = uint16_t(iaqSensor.humidity * 100.0); // compensated, in procent *100
    temp = int16_t(iaqSensor.temperature * 100.0);   // compensated, deg C *100

    compGas = iaqSensor.compGasValue;                       /*!< Reserved internal debug output */
    percentGas = uint16_t(iaqSensor.gasPercentage * 100.0); /*!< percentage of min and max filtered gas value [%] */

    gas_raw = iaqSensor._data.gas_resistance; // uint32, Gas resistance measured directly by the BME680 in Ohm
    humidity_raw = iaqSensor._data.humidity;  // uint32, Relative humidity directly measured by the BME680 in procent *1000
    pressure_raw = iaqSensor._data.pressure;  // uint32, Pressure directly measured by the BME680 in Pa.
    temp_raw = iaqSensor._data.temperature;   // int16, Temperature directly measured by BME680 in deg C *100

    rawSensorBuffer[0] = (uint8_t)((temp_raw)&0xFF);

    // copy into array
    sensorBuffer[0] = (uint8_t)((temp)&0xFF);
    sensorBuffer[1] = (uint8_t)((temp >> 8) & 0xFF);
    sensorBuffer[2] = (uint8_t)((humidity)&0xFF);
    sensorBuffer[3] = (uint8_t)((humidity >> 8) & 0xFF);
    sensorBuffer[4] = (uint8_t)((co2)&0xFF);
    sensorBuffer[5] = (uint8_t)((co2 >> 8) & 0xFF);
    sensorBuffer[6] = (uint8_t)((co2 >> 16) & 0xFF);
    sensorBuffer[7] = (uint8_t)((co2 >> 24) & 0xFF);
    sensorBuffer[8] = (uint8_t)((voc)&0xFF);
    sensorBuffer[9] = (uint8_t)((voc >> 8) & 0xFF);
    sensorBuffer[10] = (uint8_t)((voc >> 16) & 0xFF);
    sensorBuffer[11] = (uint8_t)((voc >> 24) & 0xFF);
    sensorBuffer[12] = (uint8_t)((compIAQ)&0xFF);
    sensorBuffer[13] = (uint8_t)((compIAQ >> 8) & 0xFF);
    sensorBuffer[14] = (uint8_t)((percentGas)&0xFF);
    sensorBuffer[15] = (uint8_t)((percentGas >> 8) & 0xFF);

    helpers::printToLog("__________________________________________________________________________________________________________________________________________________________________________________________________________________________________ ", verboseLevels::extraSensor);
    helpers::printToLog("Converted: \t temp: " + String(float(temp) / 100.0) + " °C\t hum: " + String(float(humidity) / 100.0) + " %\t VOC: " + String(float(voc) / 100.0) +
                            " ppm\t Comp IAQ: " + String(float(compIAQ) / 100.0) + " \t Static IAQ: " + String(float(staticIAQ) / 100.0) + " \t CO2: " + String(float(co2) / 100.0) + " ppm\t Comp Gas: " + String(compGas) + " ?\t Gas: " + String(float(percentGas) / 100.0) + " %",
                        verboseLevels::extraSensor);

    rawSensorBuffer[1] = (uint8_t)((temp_raw >> 8) & 0xFF);
    rawSensorBuffer[2] = (uint8_t)((humidity_raw)&0xFF);
    rawSensorBuffer[3] = (uint8_t)((humidity_raw >> 8) & 0xFF);
    rawSensorBuffer[4] = (uint8_t)((humidity_raw >> 16) & 0xFF);
    rawSensorBuffer[5] = (uint8_t)((humidity_raw >> 24) & 0xFF);
    rawSensorBuffer[6] = (uint8_t)((pressure_raw)&0xFF);
    rawSensorBuffer[7] = (uint8_t)((pressure_raw >> 8) & 0xFF);
    rawSensorBuffer[8] = (uint8_t)((pressure_raw >> 16) & 0xFF);
    rawSensorBuffer[9] = (uint8_t)((pressure_raw >> 24) & 0xFF);
    rawSensorBuffer[10] = (uint8_t)((gas_raw)&0xFF);
    rawSensorBuffer[11] = (uint8_t)((gas_raw >> 8) & 0xFF);
    rawSensorBuffer[12] = (uint8_t)((gas_raw >> 16) & 0xFF);
    rawSensorBuffer[13] = (uint8_t)((gas_raw >> 24) & 0xFF);
    rawSensorBuffer[14] = (uint8_t)((staticIAQ)&0xFF);
    rawSensorBuffer[15] = (uint8_t)((staticIAQ >> 8) & 0xFF);

    sensorStatusBuffer[0] = (uint8_t)((accuracyCO2)&0xFF);
    sensorStatusBuffer[1] = (uint8_t)((accuracyVOC)&0xFF);
    sensorStatusBuffer[2] = (uint8_t)((accuracyIAQ)&0xFF);
    sensorStatusBuffer[3] = (uint8_t)((accuracyPercentGAS)&0xFF);
    sensorStatusBuffer[4] = (uint8_t)((statusInitStab)&0xFF);
    sensorStatusBuffer[5] = (uint8_t)((statusPowerOnStab)&0xFF);
    sensorStatusBuffer[6] = (uint8_t)((0) & 0xFF); // placeholder, insert batteryVoltage later on
    sensorStatusBuffer[7] = (uint8_t)((0) & 0xFF); // reserved for future use

    helpers::printToLog("Raw ADC: \t temp: " + String(float(temp_raw) / 100.0) + " °C \t hum: " + String(float(humidity_raw) / 1000.0) + " % \t press: " + String(float(pressure_raw) / 100.0) + " hPa\t gas: " + String(gas_raw) + " Ω", verboseLevels::extraSensor);
    helpers::printToLog("Accuracy: \t VOC: " + accuracyLabel(accuracyVOC) + " \t IAQ: " + accuracyLabel(accuracyIAQ) + " \t CO2: " + accuracyLabel(accuracyCO2) + " \t Comp GAS: " + accuracyLabel(accuracyCompGAS) + " \t Percent GAS: " + accuracyLabel(accuracyPercentGAS) +
                            " \t Init Stabilisation: " + statusLabel(statusInitStab) + " \t Power On Stabilisation: " + statusLabel(statusPowerOnStab),
                        verboseLevels::extraSensor); //+ " \t Batt: " + String(float(batteryVoltage) / 10.0) + " V"


    // update the calibration info on the device if accuracy of the IAQ value is "high"
    updateState();
  }
  else
  {
    checkIaqSensorStatus();
    // succesRead = false; // error condition
    //helpers::printToLog("BME read failed!", verboseLevels::error, _PIN_LED_R);
  }

  return succesRead;
}

// Helper function definitions
bool BsecSupport::checkIaqSensorStatus(void)
{
  bool result = true;
  if (iaqSensor.status != BSEC_OK)
  {
    if (iaqSensor.status < BSEC_OK)
    {
      helpers::printToLog("BSEC checkIaqSensorStatus error code: " + String(iaqSensor.status), verboseLevels::error);
      result = false; // fail
    }
    else
    {
      helpers::printToLog("BSEC checkIaqSensorStatus warning code: " + String(iaqSensor.status), verboseLevels::info);
    }
  }

  if (iaqSensor.bme680Status != BME680_OK)
  {
    if (iaqSensor.bme680Status < BME680_OK)
    {
      helpers::printToLog("BSEC checkIaqSensorStatus bme680 error code: " + String(iaqSensor.bme680Status), verboseLevels::error);
      result = false; // fail
    }
    else
    {
      helpers::printToLog("BSEC checkIaqSensorStatus bme680 warning code: " + String(iaqSensor.bme680Status), verboseLevels::info);
    }
  }
  helpers::printToLog("BSEC checkIaqSensor Status code: " + String(iaqSensor.status) + "  bme680 code: " + String(iaqSensor.bme680Status), verboseLevels::extraSensor);
  iaqSensor.status = BSEC_OK;  //clear errors
  return result;
}

bool BsecSupport::loadState(void)
{
  bool result = true;
  if (EEPROM.read(0) == BSEC_MAX_STATE_BLOB_SIZE)
  {
    // Existing state in EEPROM
    helpers::printToLog("BSEC: reading existing state from EEPROM. <begin>", verboseLevels::info);

    for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE; i++)
    {
      bsecState[i] = EEPROM.read(i + 1);
      Serial.print(bsecState[i], HEX);
    }
    Serial.println("<end>"); 

    iaqSensor.setState(bsecState);

    result = checkIaqSensorStatus();

    result = setBsecConfiguration(bsecState);
  }
  else
  {   
     // No existing state present in EEPROM
    helpers::printToLog("BSEC: No existing state present in EEPROM", verboseLevels::info);
  }
  // else
  // {
  //   // Erase the EEPROM with zeroes
  //   helpers::printToLog("BSEC clearing EEPROM.", verboseLevels::info);

  //   for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE + 1; i++)
  //     EEPROM.write(i, 0);

  //   result = EEPROM.commit();
  // }

  return result;
}

bool BsecSupport::updateState(void)
{
  bool result = true;
  bool update = false;
  /* Set a trigger to save the state. Here, the state is saved every STATE_SAVE_PERIOD with the first state being saved once the algorithm achieves full calibration, i.e. iaqAccuracy = 3 */
  if (stateUpdateCounter == 0)
  {
    if (iaqSensor.iaqAccuracy >= 3) //TODO: make this configurable
    {
      update = true;
      stateUpdateCounter++;
    }
  }
  else
  {
    /* Update every STATE_SAVE_PERIOD milliseconds */
    if ((stateUpdateCounter * STATE_SAVE_PERIOD) < millis() && iaqSensor.iaqAccuracy >= 3)
    {
      update = true;
      stateUpdateCounter++;
    }
  }

  if (update)
  {
    iaqSensor.getState(bsecState);
    result = checkIaqSensorStatus();
    if (result)  //BME sensor did not indicate errors
    {
      helpers::printToLog("BSEC writing state to EEPROM. <begin>", verboseLevels::info);

      for (uint8_t i = 0; i < BSEC_MAX_STATE_BLOB_SIZE; i++)
      {
        EEPROM.write(i + 1, bsecState[i]);
        Serial.print(bsecState[i], HEX);
      }
      Serial.println("<end>");  

      EEPROM.write(0, BSEC_MAX_STATE_BLOB_SIZE);
      result = EEPROM.commit();
      if(!result)
      {
        helpers::printToLog("Error in BSEC writing state to EEPROM.", verboseLevels::error);
      }
    }
  }
  return result;
}

bool BsecSupport::setBsecConfiguration(const uint8_t bsec_config_iaq_blob[])
{
  bool result = true;
  iaqSensor.setConfig(bsec_config_iaq_blob);

  bsec_virtual_sensor_t sensorList[14] = {
      BSEC_OUTPUT_RAW_TEMPERATURE,
      BSEC_OUTPUT_RAW_PRESSURE,
      BSEC_OUTPUT_RAW_HUMIDITY,
      BSEC_OUTPUT_RAW_GAS,
      BSEC_OUTPUT_STABILIZATION_STATUS,
      BSEC_OUTPUT_RUN_IN_STATUS,
      BSEC_OUTPUT_IAQ,
      BSEC_OUTPUT_STATIC_IAQ,
      BSEC_OUTPUT_CO2_EQUIVALENT,
      BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
      BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
      BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
      BSEC_OUTPUT_COMPENSATED_GAS,
      BSEC_OUTPUT_GAS_PERCENTAGE};

  iaqSensor.updateSubscription(sensorList, 14, BSEC_SAMPLE_RATE_CONTINUOUS);
  result = checkIaqSensorStatus();
  return result;
}

// returns accuracy label of the BSEC library
String BsecSupport::accuracyLabel(int accuracy)
{
  // 0 = Stabilization / run-in ongoing, 1=low, 3=high
  String returnString = "unknown";

  if (accuracy == 0)
  {
    returnString = "Unstable";
  }
  else if (accuracy == 1)
  {
    returnString = "Low";
  }
  else if (accuracy == 2)
  {
    returnString = "Medium";
  }
  else if (accuracy == 3)
  {
    returnString = "High";
  }

  return returnString;
}

// returns status label of the BSEC library
String BsecSupport::statusLabel(int status)
{
  // stabilization  is ongoing (0) or finished (1).

  String returnString = "unknown";

  if (status == 0)
  {
    returnString = "ongoing";
  }
  else if (status == 1)
  {
    returnString = "finished";
  }

  return returnString;
}

String BsecSupport::getBsecVersion()
{
  output = "BSEC library version " + String(iaqSensor.version.major) + "." + String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + String(iaqSensor.version.minor_bugfix);

  return output;
}

//    helpers::printToLog("BME680 init failed, retrying in one second", verboseLevels::error, PIN_LED_R);

// helpers::printToLog("BME680 found at address = 0x " + String(BME680_I2C.getI2CAddress()), verboseLevels::info);

// BME680_I2C.setOversampling(TemperatureSensor, Oversample16); // Use enumerated type values
// BME680_I2C.setOversampling(HumiditySensor, Oversample16);    // Use enumerated type values
// BME680_I2C.setOversampling(PressureSensor, Oversample16);    // Use enumerated type values
// BME680_I2C.setIIRFilter(IIR4);                               // Use enumerated type values
// BME680_I2C.setGas(320, 150);                                 // 320�c for 150 milliseconds

// helpers::printToLog("BME680 set to 16x oversampling, IIR4 and gas to 320\xC2\xB0\x43 for 150ms", verboseLevels::info);

// uint16_t T1;
// int16_t T2;
// int8_t T3;
// uint16_t P1;
// int16_t P2;
// int8_t P3;
// int16_t P4;
// int16_t P5;
// int8_t P6;
// int8_t P7;
// int16_t P8;
// int16_t P9;
// uint8_t P10;
// uint16_t H1;
// uint16_t H2;
// int8_t H3;
// int8_t H4;
// int8_t H5;
// uint8_t H6;
// int8_t H7;
// int8_t G1;
// int16_t G2;
// int8_t G3;
// uint8_t res_heat_range;
// int8_t res_heat;
// int8_t rng_sw_err;
// BME680_I2C.getCalibrationValues(T1, T2, T3,
//                                 P1, P2, P3, P4, P5, P6, P7, P8, P9, P10,
//                                 H1, H2, H3, H4, H5, H6, H7,
//                                 G1, G2, G3,
//                                 res_heat_range, res_heat, rng_sw_err);

// helpers::printToLog("BME680 Calibration Coefficients: ", verboseLevels::info);
// helpers::printToLog("Temperature related: \tT1: " + String(T1) + " \tT2: " + String(T2) + " \tT3: " + String(T3), verboseLevels::info);
// helpers::printToLog("Pressure related: \t\tP1: " + String(P1) + " \tP2: " + String(P2) + " \tP3: " + String(P3) + " \t\tP4: " + String(P4) + " \tP5: " + String(P5), verboseLevels::info);
// helpers::printToLog("Pressure related: \t\tP6: " + String(P6) + " \t\tP7: " + String(P7) + " \t\tP8: " + String(P8) + " \tP9: " + String(P9) + " \tP10: " + String(P10), verboseLevels::info);
// helpers::printToLog("Humidity related: \t\tH1: " + String(H1) + " \tH2: " + String(H2) + " \tH3: " + String(H3) + " \t\tH4: " + String(H4), verboseLevels::info);
// helpers::printToLog("Humidity related: \t\tH5: " + String(H5) + " \t\tH6: " + String(H6) + " \tH7: " + String(H7), verboseLevels::info);
// helpers::printToLog("Gas heater related: \t\tG1: " + String(G1) + " \tG2: " + String(G2) + " \tG3: " + String(G3), verboseLevels::info);
// helpers::printToLog("Gas heater related: \t\tres_heat_range: " + String(res_heat_range) + " \tres_heat: " + String(res_heat) + " \trng_sw_err: " + String(rng_sw_err), verboseLevels::info);
// helpers::printToLog("End of Calibration Coefficients.", verboseLevels::info);