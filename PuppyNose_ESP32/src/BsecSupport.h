#ifndef BSECSUPPORT_H
#define BSECSUPPORT_H

#include "bsec.h"

class BsecSupport
{

public:
    // constructor
    BsecSupport(int PIN_LED_R);

    // methods
    // bool initBuffer(int bufferSize, int initialPointerPosition);
    bool checkIaqSensorStatus(void);
    // bool checkBME680_I2CStatus(void);
    bool initBsecSensor(const uint8_t I2C_address, TwoWire *i2cPort);
    bool sampleSensorData(byte sensorBuffer[], byte rawSensorBuffer[], byte sensorStatusBuffer[]);

    // getters
    // bool isBufferLocked();
    bool loadState(void);
    String accuracyLabel(int accuracy);
    String statusLabel(int status);
    String getBsecVersion();

    // setters
    // bool setPointerPosition(int pointerPosition);
    bool updateState(void);
    bool setBsecConfiguration(const uint8_t bsec_config_iaq_blob[]);

    // variable
    Bsec iaqSensor;

    byte sensorBuffer[16];
    byte rawSensorBuffer[16];
    byte sensorStatusBuffer[8];


    uint32_t voc;      // breath VOC concentration estimate [ppm] *100
    uint16_t humidity; // compensated, in procent *100
    int16_t temp;      // compensated, deg C *100

    uint16_t compIAQ; // Indoor-air-quality estimate [0-500]

    uint16_t staticIAQ; /*!< Unscaled indoor-air-quality estimate */

    uint32_t co2; /*!< co2 equivalent estimate [ppm] */

    uint8_t accuracyIAQ;        // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    uint8_t accuracyVOC;        // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    uint8_t accuracyCO2;        // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    uint8_t accuracyCompGAS;    // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    uint8_t accuracyPercentGAS; // 0 = Stabilization / run-in ongoing, 1=low, 3=high
    uint8_t accuracyCombined;   // combined, two bits per accuracy value.  order: IAQ, VOC, CO2, Percent Gas

    uint8_t statusInitStab;    // gas sensor element: initial stabilization  is ongoing (0) or finished (1).
    uint8_t statusPowerOnStab; // gas sensor element: power-on stabilization  is ongoing (0) or finished (1).
    uint8_t statusCombined;    // combined, one bit per status value.  order: Init, PowerOn

    uint32_t compGas;    /*!< Reserved internal debug output */
    uint16_t percentGas; /*!< percentage of min and max filtered gas value [%] */

    uint32_t gas_raw;      // uint32, Gas resistance measured directly by the BME680 in Ohm
    uint32_t humidity_raw; // uint32, Relative humidity directly measured by the BME680 in procent *1000
    uint32_t pressure_raw; // uint32, Pressure directly measured by the BME680 in Pa
    int16_t temp_raw;      // int16, Temperature directly measured by BME680 in deg C *100

private:
    // int _bufferSize;
    // bool _bufferLocked;
    // int _pointerPosition;
    int _PIN_LED_R;
};

#endif // BSECSUPPORT_H